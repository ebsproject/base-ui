const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');
const DotenvWebpackPlugin = require('dotenv-webpack');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'ebs',
    projectName: 'ui',
    webpackConfigEnv,
    argv,
  });

  // add to defaultConfig externals
  defaultConfig.externals.push(
    '@ebs/cs',
    '@ebs/styleguide',
    '@ebs/components',
    /@material-ui\/core.*/,
    /@material-ui\/icons.*/,
    /@material-ui\/styles.*/,
  );

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    resolve: {
      alias: {
        assets: path.resolve(__dirname, './src/assets/'),
        components: path.resolve(__dirname, './src/components/'),
        helpers: path.resolve(__dirname, './src/helpers/'),
        mock: path.resolve(__dirname, './src/mock/'),
        pages: path.resolve(__dirname, './src/pages/'),
        store: path.resolve(__dirname, './src/store/'),
        utils: path.resolve(__dirname, './src/utils/'),
      },
    },
    plugins: [new DotenvWebpackPlugin()],
  });
};
