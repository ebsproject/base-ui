module.exports = {
  testEnvironment: "jsdom",
  collectCoverageFrom: ["src/**/*.js"],
  modulePathIgnorePatterns: ["/cache", "/dist"],
  transform: {
    "^.+\\.(j|t)sx?$": "babel-jest",
    ".+\\.(css|scss|png|jpg|svg)$": "jest-transform-stub",
  },
  coverageDirectory: "coverage/",
  coverageReporters: ["json","html"],
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
    "single-spa-react/parcel": "single-spa-react/lib/cjs/parcel.cjs",
    "@ebs/cs": "<rootDir>/__mocks__/core.js",
    "@ebs/styleguide": "<rootDir>/__mocks__/styleguide.js",
    "@ebs/components": "<rootDir>/__mocks__/components.js",
  },
  moduleDirectories: ["node_modules", "src"],
  setupFilesAfterEnv: ["./jest.setup.js"],
};
