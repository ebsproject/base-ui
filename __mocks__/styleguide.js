import { forwardRef } from "react";

export const EbsDialog = forwardRef(({ children, title, ...rest }, ref) => (
  <>
    <label>{title}</label>
    {children}
  </>
));

export const EbsTabsLayout = forwardRef(({ tabs, ...rest }, ref) => (
  <div {...rest} ref={ref}>
    {tabs.map((tab, key) => (
      <div key={key}>
        <label>{tab.label}</label>
        {tab.component}
      </div>
    ))}
  </div>
));
