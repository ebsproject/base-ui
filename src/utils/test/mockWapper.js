import { MockedProvider } from '@apollo/client/testing';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { mocks } from './mockCalls';
import { store } from './mockStore';

export const Wrapper = ({ children }) => (
  <Provider store={store}>
    <IntlProvider messages={{}} locale='en' defaultLocale='en'>
      <MockedProvider mocks={mocks}>{children}</MockedProvider>
    </IntlProvider>
  </Provider>
);
