import axios from 'axios';

export const client = axios.create({
  baseURL: process.env.REACT_APP_CSAPI_URI_REST,
  headers: {
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

client.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem('id_token')}`;
  return config;
});
