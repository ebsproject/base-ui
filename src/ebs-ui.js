import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from "single-spa-react";
import _App from "./root.component";

const lifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: _App,
  errorBoundary(err, info, props) {
    // Customize the root error boundary for your microfrontend here.
    return null;
  },
});
export const App = _App;
export const { bootstrap, mount, unmount } = lifecycles;
