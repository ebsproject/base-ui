  import Table from './Table';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Table is in the DOM', () => {
  render(<Table></Table>)
  expect(screen.getByTestId('TableTestId')).toBeInTheDocument();
})
