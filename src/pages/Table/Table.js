import React from 'react'
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl'
import { EbsGrid } from '@ebs/components'
// CORE COMPONENTS
import { IconButton, Typography } from "@material-ui/core";
import { Add } from '@material-ui/icons';
    
//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Table = React.forwardRef(({ }, ref) => {
    // Columns
    const columns = [
        { Header: "Id", accessor: "id", hidden: true },
        { 
            Header: (
                <Typography variant='h5' color='primary'>
                    <FormattedMessage id='none' defaultMessage='Label'/>
                </Typography>
            ),
            Cell: ({ value }) => {
                return <Typography variant='body1'>{value}</Typography>
            },
            accessor: "key",
            csvHeader: 'Label for CSV file',
            width: 500,
        },
    ];
    
/*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
const toolbarActions = (selectedRows, refresh) => {
    return (
        <IconButton
            title="title"
            onClick={() => {
                alert(selectedRows);
                refresh();
            }}
            color="inherit"
        >
            <AddIcon/>
        </IconButton>
    );
};
    
/*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
const rowActions = (rowData, refresh) => {
    return (
        <IconButton
            title="title"
            onClick={() => {
                alert(JSON.stringify(rowData));
                refresh();
            }}
            color="inherit"
        >
            <AddIcon/>
        </IconButton>
    );
};
    
/* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
return (      
    <EbsGrid
        toolbar={true}
        data-testid={'TableGridTestId'}
        title="Your Title"
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        uri="http://localhost:18080/graphql"
        entity="YourEntity"
        callstandard="graphql"
        height='85vh'
        select='multi'
    />
);
});
    
// Type and required properties
Table.propTypes = {}
// Default properties
Table.defaultProps = {}
      
export default Table;
