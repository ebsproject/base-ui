import { combineReducers } from 'redux';
import Example from 'store/modules/Example';

export default combineReducers({ Example });
