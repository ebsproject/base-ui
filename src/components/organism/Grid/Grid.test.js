import Grid from './Grid';
import React from 'react';
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);

test('Grid is in the DOM', () => {
  render(<Grid></Grid>);
  expect(screen.getByTestId('GridTestId')).toBeInTheDocument();
});
