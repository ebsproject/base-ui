import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import { Button, Typography } from '@material-ui/core';

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ButtonAtom = React.forwardRef(({ }, ref) => {
  
  return (
    /* 
     @prop data-testid: Id to use inside Button.test.js file.
     */
    <Button
      data-testid={'ButtonTestId'}
      ref={ref}
      variant='contained'
      className='w-224 bg-ebs-brand-default hover:bg-ebs-brand-900 text-white'
      aria-label='Login'
    >
      <Typography variant='button'>
        <FormattedMessage id='none' defaultMessage='My button' />
      </Typography>
    </Button>
  )
})
// Type and required properties
ButtonAtom.propTypes = {}
// Default properties
ButtonAtom.defaultProps = {}

export default ButtonAtom
