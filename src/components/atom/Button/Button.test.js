  import Button from './Button';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Button is in the DOM', () => {
  render(<Button></Button>)
  expect(screen.getByTestId('ButtonTestId')).toBeInTheDocument();
})
