  import Flex from './Flex';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Flex is in the DOM', () => {
  render(<Flex></Flex>)
  expect(screen.getByTestId('FlexTestId')).toBeInTheDocument();
})
