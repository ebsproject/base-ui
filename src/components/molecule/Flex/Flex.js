import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import { Box, Typography } from '@material-ui/core';

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FlexMolecule = React.forwardRef(({ }, ref) => {

  return (
    /* 
     @prop data-testid: Id to use inside Flex.test.js file.
     */
    <Box
      component='div' 
      ref={ref}
      data-testid={'FlexTestId'}
      className='flex flex-row bg-ebs-brand-default'
    >
      <Box component='div' className="w-1/2 bg-ebs-brand-900">
        <Typography variant='body1'>
          <FormattedMessage id='none' defaultMessage='My molecule' />
        </Typography>
      </Box>
    </Box>
  )
})
// Type and required properties
FlexMolecule.propTypes = {}
// Default properties
FlexMolecule.defaultProps = {}

export default FlexMolecule
