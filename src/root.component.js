import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter, Route } from 'react-router-dom';
import { EBSMaterialUIProvider } from '@ebs/styleguide';
import { IntlProvider } from 'react-intl';
import store from './store';
import { client } from 'utils/apollo';
// Routes
import { BASEPATH } from './routes';

export default function App() {
  const [locale, setLocale] = useState('en');
  const [messages, setMessages] = useState(null);

  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <ApolloProvider client={client}>
          <IntlProvider locale={locale} messages={messages} defaultLocale='en'>
            <BrowserRouter>
              <Route
                path={BASEPATH}
                render={({ match }) => (
                  <>
                    <Route
                      exact
                      path={match.url}
                      component={()=>(<div>Hello world!</div>)}
                    />
                  </>
                )}
              />
            </BrowserRouter>
          </IntlProvider>
        </ApolloProvider>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
